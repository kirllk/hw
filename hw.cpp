#include <iostream>
#include <string>
#include <cassert>
#include <sstream>
#include <utility>
#include "collector.h"

const int MAX_NAME = 50;
const int MAX_SURNAME = 50;
const int MAX_LASTNAME = 50;
const int MIN_ID = 1;
const int MIN_YEAR = 1900;
const int MAX_YEAR = 2300;
const int MIN_RATING = 0;
const int MAX_RATING = 100;
const int MAX_KAF = 10;

enum AcademicDegree {
  assistant_professor,
  professor,
  senior_lecturer,
  teacher,
  graduate_student,
  master,
  bachelor
};


class ElectronicJournal: public ICollectable {
 protected:
  bool invariant() const {
	return !_name.empty() && !_surname.empty() && !_lastname.empty() && !_kaf.empty() &&
	_name.size() <= MAX_NAME && _surname.size() <= MAX_SURNAME && _lastname.size() <= MAX_LASTNAME &&
	_id >= MIN_ID && _year >= MIN_YEAR && _year <= MAX_YEAR && _rating >= MIN_RATING && _rating <= MAX_RATING &&
	_kaf.size() <= MAX_KAF;
  }

 public:
  ElectronicJournal() = delete;
  ElectronicJournal(const ElectronicJournal& p) = delete;
  ElectronicJournal& operator=(const ElectronicJournal& p) = delete;

  ElectronicJournal(std::string name, std::string surname, std::string lastname, int id, int year, std::string kaf, AcademicDegree academicDegree, int rating):
  _name(std::move(name)), _surname(std::move(surname)), _lastname(std::move(lastname)), _id(id), _year(year), _kaf(std::move(kaf)), _academicDegree(academicDegree), _rating(rating)
  {
  assert(invariant());
  }

  std::string getFIO() const {
    return _name + " " + _surname + " " + _lastname;
  }

  int getId() const {
    return _id;
  }

  int getYear() const {
	return _year;
  }

  int getRating() const {
	return _rating;
  }

  const std::string& getKaf() const {
	return _kaf;
  }

  std::string getAcademicDegree() const {
	if (_academicDegree == AcademicDegree::assistant_professor) {
	  return "Доцент";
	}
	if (_academicDegree == AcademicDegree::bachelor) {
	  return "Бакалавр";
	}
	if (_academicDegree == AcademicDegree::graduate_student) {
	  return "Аспирант";
	}
	if (_academicDegree == AcademicDegree::master) {
	  return "Магистр";
	}
	if (_academicDegree == AcademicDegree::professor) {
	  return "Профессор";
	}
	if (_academicDegree == AcademicDegree::senior_lecturer) {
	  return "Старший преподаватель";
	}
	if (_academicDegree == AcademicDegree::teacher) {
	  return "Преподаватель";
	}
	return "";
  }


  bool write(std::ostream& os) override {
	writeString(os, _name);
	writeString(os, _surname);
	writeString(os, _lastname);
	writeNumber(os, _id);
	writeNumber(os, _year);
	writeString(os, _kaf);
	writeNumber(os, _academicDegree);
	writeNumber(os, _rating);
	return os.good();
  }

 private:
  std::string _name;
  std::string _surname;
  std::string _lastname;
  int _id;
  int _year;
  std::string _kaf;
  AcademicDegree _academicDegree;
  int _rating;
};

AcademicDegree getAcademicDegree(const std::string& str) {
  if (str == "Доцент") {
	return AcademicDegree::assistant_professor;
  }
  if (str == "Бакалавр") {
	return AcademicDegree::bachelor;
  }
  if (str == "Аспирант") {
	return AcademicDegree::graduate_student;
  }
  if (str == "Магистр") {
	return AcademicDegree::master;
  }
  if (str == "Профессор") {
	return AcademicDegree::professor;
  }
  if (str == "Старший преподаватель") {
	return AcademicDegree::senior_lecturer;
  }
  if (str == "Преподаватель") {
	return AcademicDegree::teacher;
  }
  return AcademicDegree();
}


class ItemCollector: public ACollector {
 public:
  std::shared_ptr<ICollectable> read(std::istream& is) override {
	std::string name = readString(is, MAX_NAME);
	std::string surname = readString(is, MAX_SURNAME);
	std::string lastname = readString(is, MAX_LASTNAME);
	int id = readNumber<int>(is);
	int year = readNumber<int>(is);
	std::string kaf = readString(is, MAX_KAF);
	auto academicDegree = readNumber<AcademicDegree>(is);
	int rating = readNumber<int>(is);
	return std::make_shared<ElectronicJournal>(name, surname, lastname, id, year, kaf, academicDegree, rating);
  }
};

bool performCommand(const std::vector<std::string> & args, ItemCollector & col) {
  if (args.empty()) {
	return false;
  }

  if (args[0] == "l" || args[0] == "load") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!col.loadCollection(filename)) {
	  std::cerr << "Ошибка при загрузке файла '" << filename << "'" << std::endl;
	  return false;
	}
	return true;
  }

  if (args[0] == "s" || args[0] == "save") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!col.saveCollection(filename)) {
	  std::cerr << "Ошибка при сохранении файла '" << filename << "'" << std::endl;
	  return false;
	}
	return true;
  }

  if (args[0] == "c" || args[0] == "clean") {
	if (args.size() != 1) {
	  std::cerr << "Некорректное количество аргументов команды clean" << std::endl;
	  return false;
	}
	col.clean();
	return true;
  }

  if (args[0] == "a" || args[0] == "add") {
	if (args.size() != 9) {
	  std::cerr << "Некорректное количество аргументов команды add" << std::endl;
	  return false;
	}

	col.addItem(std::make_shared<ElectronicJournal>(args[1], args[2], args[3], std::stoi(args[4]), std::stoi(args[5]), args[6], getAcademicDegree(args[7]), std::stoi(args[8])));
	return true;
  }

  if (args[0] == "r" || args[0] == "remove") {
	if (args.size() != 2) {
	  std::cerr << "Некорректное количество аргументов команды remove" << std::endl;
	  return false;
	}
	col.removeItem(std::stoi(args[1]));
	return true;
  }

  if (args[0] == "u" || args[0] == "update") {
	if (args.size() != 10) {
	  std::cerr << "Некорректное количество аргументов команды update" << std::endl;
	  return false;
	}
	col.updateItem(stoul(args[1]), std::make_shared<ElectronicJournal>(args[2], args[3], args[4], std::stoi(args[5]), std::stoi(args[6]), args[7], getAcademicDegree(args[8]), std::stoi(args[9])));
	return true;
  }

  if (args[0] == "v" || args[0] == "view") {
	if (args.size() != 1) {
	  std::cerr << "Некорректное количество аргументов команды view" << std::endl;
	  return false;
	}

	size_t count = 0;
	for(size_t i = 0; i < col.getSize(); ++i) {
	  const ElectronicJournal & item = dynamic_cast<ElectronicJournal &>(*col.getItem(i));
	  if (!col.isRemoved(i)) {
		std::cout << "[" << i << "]"
		<< " ФИО студента: " << item.getFIO()
		<< " ID студента: " << item.getId()
		<< " год поступления: " << item.getYear()
		<< " кафедра: " << item.getKaf()
		<< " учёная степень : " << item.getAcademicDegree()
		<< " рейтинг: " << item.getRating() << std::endl;
		count ++;
	  }
	}

	std::cout << "Количество элементов в коллекции: " << count << std::endl;
	return true;
  }

  std::cerr << "Недопустимая команда '" << args[0] << "'" << std::endl;
  return false;
}

int main(int , char **) {
  ItemCollector col;

  for (std::string line; getline(std::cin, line); ) {
	if (line.empty()) {
	  break;
	}

	std::istringstream  iss(line);
	std::vector<std::string> args;

	for(std::string str; iss.good();) {
	  iss >> str;
	  args.emplace_back(str);
	}

	if (!performCommand(args, col)) {
	  return 1;
	}
  }

  std::cout << "Выполнение завершено успешно" << std::endl;
  return 0;
}
